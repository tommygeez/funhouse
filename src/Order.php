<?php
    namespace FunHouse;

    class Order
    {
        protected $products = [];

        public function products()
        {
            return $this->products;
        }

        public function add(Product $product)
        {
            $this->products[] = $product;
        }

        public function totalPrice(): Int
        {
            return collect($this->products)->sum(function ($product) { return $product->price(); });
        }
    }
