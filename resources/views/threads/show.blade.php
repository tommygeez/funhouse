@extends('layouts.app')

@section('content')
    <div class="container">
        @if (auth()->check())
            <div class="row">
                <br /><br /><br />

                <form method="POST" action="{{ route('thread.add_reply', [$thread->channel->name, $thread]) }}">
                    {{ csrf_field() }}
                    <textarea name="description"></textarea>
                    <input name="title" />

                    <input type="submit" value="Submit" />
                </form>
            </div>

            @else
            <p>Please sign in to post a reply ->> <a href="{{ route('login')}}">Login</a></p>
        @endif

        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Show Thread - {{ $thread->title }}</div>
                    <div class="card-body">
                        <a href="#">AAAAAA{{ $thread->owner->name }}</a>
                       {{ $thread->title }}

                        <hr />
                        {{ $thread->description }}
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            @foreach ($thread->replies as $reply)
                @include ('threads.reply')
            @endforeach
        </div>


    </div>
@endsection
