<?php

namespace App\Http\Controllers;

use App\Thread;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class RepliesController extends Controller
{
    function __construct()
    {
        $this->middleware('auth');
    }

    public function store($channelId, Thread $thread)
    {
        $thread->addReply(
            [
                'user_id' => Auth::id(),
                'title' => \request('title'),
                'description' => \request('description')
            ]
        );

        return back();
    }
}
