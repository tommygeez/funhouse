<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class CreateThreadTest extends TestCase
{
    use DatabaseMigrations;

    /** @test */
    public function guests_cannot_create_threads()
    {
       $this->withExceptionHandling();

        $this->get('/threads/create')
            ->assertRedirectedTo('/login');

        $this->post('/threads')
            ->assertRedirectedTo('/login');
    }

    /** @test */
    public function an_authenticated_user_can_create_thread()
    {
        $this->signIn();

        $thread = create('App\Thread');
        $this->post('/threads', $thread->toArray());

        $this->get($thread->path())
            ->see($thread->description);
    }
}
