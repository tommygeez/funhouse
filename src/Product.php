<?php
    namespace FunHouse;

    class Product
    {
        protected $name, $price;

        function __construct($name, $price = 0)
        {
            $this->name = $name;
            $this->price = $price;
        }

        public function name()
        {
            return $this->name;
        }

        public function price()
        {
            return $this->price;
        }
    }
