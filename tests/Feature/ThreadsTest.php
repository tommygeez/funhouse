<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ThreadsTest extends TestCase
{
    use DatabaseMigrations;

    protected $thread;

    public function setUp(): void
    {
        parent::setUp();
        $this->thread = factory('App\Thread')->create();
    }

    /** @test  */
    public function a_user_can_see_all_threads()
    {
        $this->get('/threads')
            ->see($this->thread->title);
    }

    /** @test */
    public function a_user_can_see_specific_thread()
    {
        $this->get($this->thread->path())
            ->see($this->thread->title);
    }

    /** @test */
    public function a_user_can_read_replies_that_are_associated_with_a_thread()
    {
        // if that thread have replies
        $reply = factory('App\Reply')
            ->create(['thread_id' => $this->thread->id]);

        $this->get($this->thread->path())
            ->assertResponseStatus(200)
            ->see($reply->description);
    }
}
