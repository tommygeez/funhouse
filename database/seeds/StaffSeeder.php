<?php

    use Illuminate\Database\Seeder;
    use Illuminate\Support\Facades\DB;
    use App\Shop;

    class StaffSeeder extends Seeder
    {

        private $shopId = null;

        private function getFirstShopId()
        {
           return $this->shopId === null ? Shop::first()->id : 1;
        }

        private function getCurrentTimestamp()
        {
            return date('Y-m-d H:i:s') ?: null;
        }
        /**
         * Run the database seeds.
         *
         * @return void
         */
        public function run()
        {
            $staff = [[
                'first_name' => 'Thor',
                'surname' => 'Marvel',
                'shop_id' => $this->getFirstShopId(),
                'created_at' => $this->getCurrentTimestamp(),
                'updated_at' => $this->getCurrentTimestamp()
            ], [
                'first_name' => 'Black Widow',
                'surname' => 'Marvel',
                'shop_id' => $this->getFirstShopId(),
                'created_at' => $this->getCurrentTimestamp(),
                'updated_at' => $this->getCurrentTimestamp()
            ], [
                'first_name' => 'Wolverine',
                'surname' => 'Marvel',
                'shop_id' => $this->getFirstShopId(),
                'created_at' => $this->getCurrentTimestamp(),
                'updated_at' => $this->getCurrentTimestamp()
            ], [
                'first_name' => 'Gamora',
                'surname' => 'Marvel',
                'shop_id' => $this->getFirstShopId(),
                'created_at' => $this->getCurrentTimestamp(),
                'updated_at' => $this->getCurrentTimestamp()
            ]];

            collect($staff)->map(function ($staffMember) { DB::table('staff')->updateOrInsert($staffMember); });
        }
    }
