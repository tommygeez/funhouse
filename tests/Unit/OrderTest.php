<?php

namespace Tests\Unit;

use FunHouse\Order;
use FunHouse\Product;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class OrderTest extends TestCase
{
    protected $order;

    public function setUp(): void
    {
        $this->order = $this->createOrderWithProducts();
    }

    /** @test */
    public function order_has_products()
    {
        $this->assertNotEmpty($this->order->products());
        $this->assertCount(2, $this->order->products());
    }

    /** @test */
    public function order_products_total()
    {
        $this->assertEquals(100, $this->order->totalPrice());
    }

    protected function createOrderWithProducts()
    {
        $order = new Order();

        $product = new Product('Fallout', 50);
        $product2 = new Product('OMG', 50);

        $order->add($product);
        $order->add($product2);

        return $order;
    }
}
