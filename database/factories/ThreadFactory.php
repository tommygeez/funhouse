<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use Faker\Generator as Faker;

$factory->define(App\Thread::class, function (Faker $faker) {
    return [
        'title' => $faker->sentence(),
        'description' => $faker->paragraph(),
        'channel_id' => function () {
            return factory(App\Channel::class)->create()->id;
        },
        'user_id' => function () {
            return factory(App\User::class)->create()->id;
        }
    ];
});

$factory->define(App\Reply::class, function (Faker $faker) {
    return [
        'title' => $faker->sentence(),
        'description' => $faker->paragraph(),
        'user_id' => function () {
            return factory(App\User::class)->create()->id;
        },
        'thread_id' => function () {
            return factory(App\Thread::class)->create()->id;
        }
    ];
});

    $factory->define(App\Channel::class, function (Faker $faker) {

        $name = $faker->word;

        return [
            'name' => $name,
            'slug' => $name
        ];
    });
