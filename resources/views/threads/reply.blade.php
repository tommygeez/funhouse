<div class="panel">
    <h2>{{ $reply->owner->name }}</h2>
    <h5>{{ $reply->created_at->diffForHumans() }}</h5>
    {{ $reply->description }}
</div>
